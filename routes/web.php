<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Psr\Http\Message\ServerRequestInterface;

Route::group(['domain' => '{account}.'.env('APP_DOMAIN')], function () {
    Route::get('/', 'ProfileController@profile')->name('profile');
    Route::get('/profile/save_click/{media_id}', 'ProfileController@save_click')->name('.profilesave_click');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', function () {
    return redirect('/login');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'api'], function () {

    Route::get('/fakeapicall/product={products}', 'HomeController@fakeapi')->name('fakeapi');

});

Route::group(['prefix' => 'panel'], function () {
    $this->middleware('auth:api');
    Route::get('/', 'PanelController@index')->name('homepanel');
    Route::get('/edit', 'PanelController@edit')->name('edit');
    Route::get('/settings', 'PanelController@settings')->name('settings');
    Route::get('/setlocale/{type}/{lang}', 'PanelController@setlocale')->name('setlocale');
});

Route::group(['prefix' => 'instagram'], function () {
    $this->middleware('auth:api');

    //Media Instagram
    Route::get('/togglemedia/{media_id}', 'InstagramController@toggleMedia', function (ServerRequestInterface $request) {})->name('instagram.hide_photo');
    Route::get('/refreshmedia', 'InstagramController@refreshmedia', function (ServerRequestInterface $request) {})->name('instagram.refreshmedia');
    Route::post('/save_custom_url/', 'InstagramController@save_custom_url', function (ServerRequestInterface $request) {})->name('instagram.save_custom_url');
    Route::post('/save_auto_link_url/', 'InstagramController@save_auto_link_url', function (ServerRequestInterface $request) {})->name('instagram.save_auto_link_url');
    Route::get('/auto_link/', 'InstagramController@auto_link', function (ServerRequestInterface $request) {})->name('instagram.auto_link');
    Route::get('/match_tag/', 'InstagramController@match_tag', function (ServerRequestInterface $request) {})->name('instagram.match_tag');
    Route::post('/save_custom_tag/', 'InstagramController@save_custom_tag', function (ServerRequestInterface $request) {})->name('instagram.save_custom_tag');

    // Logowanie insta
    Route::get('/unconnect', 'InstagramController@unconnect', function (ServerRequestInterface $request) {})->name('instagram.unconnect');
    Route::get('/connect', 'InstagramController@connect', function (ServerRequestInterface $request){})->name('insta.connect');
    Route::get('/login', 'InstagramController@login', function (ServerRequestInterface $request){})->name('insta.login');

});


Route::get('register/verify/{token}', 'Auth\RegisterController@verify');
Route::get('/logout', 'HomeController@logout')->name('logout');