<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Instagrammedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->bigInteger('insta_id')->unsigned();
            $table->string('media_id')->unique();
            $table->string('code');
            $table->longText('image');
            $table->string('created_time');
            $table->string('hashtags')->nullable();
            $table->integer('likes');
            $table->string('can_view_comments');
            $table->string('can_delete_comments');
            $table->string('type');
            $table->string('link');
            $table->string('custom_link')->nullable();
            $table->integer('hidden')->nullable();
            $table->string('location')->nullable();
            $table->string('alt_media_url')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_media');
    }
}
