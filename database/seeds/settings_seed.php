<?php

use Illuminate\Database\Seeder;

class settings_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'auto_link' => null,
            'match_tag' => null,
            'auto_link_url' => null

        ]);
    }
}
