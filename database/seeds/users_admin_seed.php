<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class users_admin_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123'),
            'verified' => '1'
        ]);
    }
}
