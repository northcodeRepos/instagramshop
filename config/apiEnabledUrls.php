<?php

return [

    // Api classes are added to /app/Classes/ folder.
    // Example api class :
    //    class ExampleApi extends ApiAbstract {
    //
    //        /**
    //         * @param array $products
    //         * @param mixed $lang
    //         * @return mixed
    //         * @throws Exception
    //         */
    //        public function getProducts($products = [], $lang = false) {
    //            try {
    //                $postData = $this->configuration()['post_data'];
    //                $postData['id'] = join(',', $products);
    //                if($lang !== false) {
    //                    $postData['store'] = $lang;
    //                }
    //
    //                $client = new Client();
    //                $res = $client->request($this->configuration()['method'], $this->configuration()['url'], ['form_params' => $postData]);
    //
    //                if($res->getStatusCode() === 200) {
    //                    return json_decode($res->getBody());
    //                }
    //            } catch (\Exception $e) {
    //                throw $e;
    //            }
    //        }
    //
    //        public function configuration() {
    //            return [
    //                'url' => 'http://example.com/instagram_api.php',
    //                'method' => 'POST',
    //                'post_data' => [
    //                    'auth' => '----authcode----',
    //                    'store' => 'fr' //fallback language
    //                ]
    //            ];
    //        }
    //    }

    // Now you need to simply add new class to our list, like :
    //    'example' => [
    //        'url' => 'example.com/instagram_api.php',
    //        'class' => ExampleApi::class
    //    ],

    'lilou' => [
        'url' => 'liloudemo.m4u-dev.it/instagram_api.php',
        'class' => LilouApi::class
    ]
];

