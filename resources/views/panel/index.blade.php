<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>Sklep instagram | Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components"
          type="text/css"/>
    <link href="{{asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{asset('assets/layouts/layout4/css/layout.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/layouts/layout4/css/themes/light.min.css') }}" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="{{asset('assets/layouts/layout4/css/custom.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.2/sweetalert2.min.css">

</head>

<style>
    .insta_photo {
        margin-bottom: 30px;
    }

    .no_padding {
        padding: 0;
    }

    .swal2-container {
        z-index: 9999999;
    }

    .swal2-icon.swal2-success .swal2-success-fix {
        display: none;
    }

    .swal2-icon.swal2-success [class^=swal2-success-circular-line] {
        width: initial !important;
    }
    html body .dashboard-stat2 {
        padding-bottom: 0;
    }
    .insta_photo {
        overflow: hidden;
        position: relative;}
    .insta_photo .stats {
        position: absolute;
        width: 100px;
        text-align: center;
        height: 30px;
        top: 0;
        margin: auto;
        background: rgba(0, 0, 0, 0.74);
        left: 15px;
        color: #fff;
        font-size: 15px;
        border-bottom-right-radius: 5px !important;
        line-height: 30px;
    }
</style>
<!-- END HEAD -->
<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">

<input type="hidden" name="_token" value="{{ csrf_token() }}">

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{route('home')}}">
                <img src="{{asset('images/logo.png')}}" alt="logo" class="logo-default"/> </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <!-- DOC: Remove "hide" class to enable the page header actions -->
        <!-- END PAGE ACTIONS -->
        <!-- BEGIN PAGE TOP -->

        <div class="page-top">
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" style="cursor: context-menu !important;" class="dropdown-toggle"
                           data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="username username-hide-on-mobile"> @if(isset(Auth::user()->instagram)){{Auth::user()->instagram->full_name}} @else {{Auth::user()->name}} @endif</span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle"
                                 src="@if(isset(Auth::user()->instagram)){{Auth::user()->instagram->profile_picture}}@else {{asset('assets/layouts/layout4/img/avatar9.jpg')}} @endif"/>
                        </a>

                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->

                    <!-- END QUICK SIDEBAR TOGGLER -->

                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- BEGIN SIDEBAR -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="nav-item start active open">
                    <a href="/" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item start @if(Route::currentRouteName() == 'homepanel') active @endif">
                            <a href="{{route('home')}}" class="nav-link ">
                                <i class="icon-home"></i>
                                <span class="title"> <?php echo __('panel/nav.homepage'); ?></span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        <li class="nav-item start @if(Route::currentRouteName() == 'settings') active @endif">
                            <a href="{{route('settings')}}" class="nav-link ">
                                <i class="icon-wrench"></i>
                                <span class="title"><?php echo __('panel/nav.settings'); ?></span>
                                <span class="selected"></span>
                            </a>
                        </li>
                        @if(isset(Auth::user()->instagram))
                            <li class="nav-item start @if(Route::currentRouteName() == 'edit') active @endif">
                                <a href="{{route('edit')}}" class="nav-link ">
                                    <i class="icon-settings"></i>
                                    <span class="title"><?php echo __('panel/nav.edit'); ?></span>
                                    <span class="selected"></span>
                                </a>
                            </li>

                            <li class="nav-item start ">
                                <a href="{{ route('profile', ['account' => Auth::user()->instagram->username]) }}" target="_blank" class="nav-link ">
                                    <i class="icon-user"></i>
                                    <span class="title"><?php echo __('panel/nav.my_profile'); ?></span>
                                </a>
                            </li>
                        @else

                            <li class="nav-item start">
                                <a href="javascript:void(0);" style="opacity: 0.3; cursor: not-allowed;" class="nav-link ">
                                    <i class="icon-settings"></i>
                                    <span class="title"><?php echo __('panel/nav.edit'); ?></span>
                                    <span class="selected"></span>
                                </a>
                            </li>

                            <li class="nav-item start ">
                                <a href="javascript:void(0);" style="opacity: 0.3; cursor: not-allowed;" class="nav-link ">
                                    <i class="icon-user"></i>
                                    <span class="title"><?php echo __('panel/nav.my_profile'); ?></span>
                                </a>
                            </li>
                        @endif
                        <li class="nav-item start ">
                            <a href="{{route('logout')}}" class="nav-link ">
                                <i class="icon-key"></i>
                                <span class="title"><?php echo __('auth.logout'); ?></span>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
        <!-- END SIDEBAR -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            @if(isset(Auth::user()->instagram) && Route::currentRouteName() == 'homepanel')
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 bordered">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <span data-counter="counterup" >{{$visits}}</span>
                                    <small class="font-green-sharp"></small>
                                </h3>
                                <small><?php echo __('panel/panel.views'); ?></small>
                            </div>
                            <div class="icon">
                                <i class="icon-pie-chart"></i>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            @endif
            <!-- BEGIN PAGE HEAD-->

            <!-- END DASHBOARD STATS 1-->
        @yield('content')

        <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->
<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="../assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
<script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.2/sweetalert2.min.js"></script>
<script>
    if ($('#refresmedia').length) {
        $('#refresmedia').on('click', function () {
            $('#refresmedia').html('<?php echo __('panel/panel.wait'); ?>');
            $.get('/instagram/refreshmedia', function (data) {
                if(JSON.parse(data) == 'private_profile') {
                    swal(
                        'Oops...',
                        '<?php echo __('panel/panel.info-private-refresh'); ?>',
                        'error'
                    )
                    $('#refresmedia').html('<?php echo __('panel/panel.refresh_photos'); ?>');
                }
                if (data == 'true') {
                    location.reload();
                } else if (data == 'false'){
                    swal(
                        'Oops...',
                        '<?php echo __('panel/panel.error'); ?>',
                        'error'
                    )
                    $('#refresmedia').html('<?php echo __('panel/panel.refresh_photos'); ?>');
                }
            });


        });
    }

    if ($('#unconnect').length) {
        $('#unconnect').on('click', function () {
            $('#unconnect').html('<?php echo __('panel/panel.wait'); ?>');
            $.get('/instagram/unconnect', function (data) {
                if (data == 'true') {
                    location.reload();
                } else {
                    swal(
                        'Oops...',
                        '<?php echo __('panel/panel.error'); ?>',
                        'error'
                    )
                }
            });


        });
    }


    if ($('.save_custom_url').length) {
        $('.save_custom_url').on('click', function () {
            var photo_button = $(this);
            photo_button.html('<?php echo __('panel/panel.wait'); ?>');
            var custom_url = $(this).parent().parent().find('.custom_url').val();
            var media_id = photo_button.attr('media_id');
            if (custom_url.length < 5) {
                swal(
                    'Oops...',
                    '<?php echo __('panel/panel.link'); ?>',
                    'error'
                );
                photo_button.html('<?php echo __('panel/panel.save'); ?>');
                return;

            }
            $.ajax({
                type: 'post',
                url: '/instagram/save_custom_url',
                dataType: 'json',
                data: {
                    url: custom_url,
                    media_id: media_id
                },
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                },
                success: function (data) {
                    photo_button.html('<?php echo __('panel/panel.save'); ?>');
                    if (data == true) {

                        swal(
                            '',
                            '<?php echo __('panel/panel.link-saved'); ?>',
                            'success'
                        );
                    } else if (data == false) {
                        swal(
                            'Oops...',
                            '<?php echo __('panel/panel.error'); ?>',
                            'error'
                        )
                    } else {
                        swal(
                            'Oops...',
                            data,
                            'error'
                        )
                    }
                },
                error: function (data) {
                    alert('Error:', data);
                }
            });

        });

    }
    if ($('.save_auto_link_url').length) {
        $('.save_auto_link_url').on('click', function () {
            var photo_button = $(this);
            photo_button.html('<?php echo __('panel/panel.wait'); ?>');
            var custom_url = $(this).parent().parent().find('.auto_link_url').val();
            if (custom_url.length < 5) {
                swal(
                    'Oops...',
                    '<?php echo __('panel/panel.link'); ?>',
                    'error'
                );
                photo_button.html('<?php echo __('panel/panel.save'); ?>');
                return;

            }
            $.ajax({
                type: 'post',
                url: '/instagram/save_auto_link_url',
                dataType: 'json',
                data: {
                    url: custom_url
                },
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                },
                success: function (data) {
                    photo_button.html('<?php echo __('panel/panel.save'); ?>');
                    if (data == true) {

                        swal(
                            '',
                            '<?php echo __('panel/panel.link-saved'); ?>',
                            'success'
                        );
                    } else if (data == false) {
                        swal(
                            'Oops...',
                            '<?php echo __('panel/panel.class_not_found'); ?>',
                            'error'
                        )
                    } else {
                        swal(
                            'Oops...',
                            data,
                            'error'
                        )
                    }

                },
                error: function (data) {
                    alert('Error:', data);
                }
            });

        });

    }
    if ($('.show_hide_photo').length) {
        $('.show_hide_photo').on('click', function () {
            var photo_button = $(this);
            photo_button.html('<?php echo __('panel/panel.wait'); ?>');
            $.get('/instagram/togglemedia/' + photo_button.attr('media_id'), function (data) {
                data = JSON.parse(data);
                photo_button.html('<?php echo __('panel/panel.show_hide'); ?>');
                if (data != 'null') {
                    photo_button.parent().parent().find('.photo_status').html('<?php echo __('panel/panel.invisible'); ?>').attr('style', 'color:red;')
                } else {
                    photo_button.parent().parent().find('.photo_status').html('<?php echo __('panel/panel.visible'); ?>').attr('style', 'color:green;')
                }
            });


        });
    }
    if ($('.auto_link').length) {
        $('.auto_link').on('click', function () {
            var photo_button = $(this);
            photo_button.html('<?php echo __('panel/panel.wait'); ?>');
            $.get('/instagram/auto_link/', function (data) {
                data = JSON.parse(data);
                photo_button.html('<?php echo __('panel/panel.on'); ?>');
                if (data != 'null') {
                    photo_button.removeClass('btn-success').addClass('btn-danger').html('<?php echo __('panel/panel.off'); ?>');
                    $('.auto_link_url').removeProp('readonly');
                    $('.save_auto_link_url').removeProp('disabled');
                } else {
                    photo_button.removeClass('btn-danger').addClass('btn-success').html('<?php echo __('panel/panel.on'); ?>');
                    $('.auto_link_url').attr('readonly', 'readonly');
                    $('.save_auto_link_url').attr('disabled', 'disabled');
                }

            });


        });
    }
    if ($('.match_tag').length) {
        $('.match_tag').on('click', function () {
            var photo_button = $(this);
            photo_button.html('<?php echo __('panel/panel.wait'); ?>');
            $.get('/instagram/match_tag/', function (data) {
                data = JSON.parse(data);
                photo_button.html('<?php echo __('panel/panel.on'); ?>');
                if (data != 'null') {
                    photo_button.removeClass('btn-success').addClass('btn-danger').html('<?php echo __('panel/panel.off'); ?>');
                    photo_button.parent().find('.custom_tag').show();
                    photo_button.parent().find('.save_custom_tag').show();
                } else {
                    photo_button.removeClass('btn-danger').addClass('btn-success').html('<?php echo __('panel/panel.on'); ?>');
                    photo_button.parent().find('.custom_tag').hide();
                    photo_button.parent().find('.save_custom_tag').hide();
                }
            });


        });
    }
    if ($('.save_custom_tag').length) {
        $('.save_custom_tag').on('click', function () {
            var photo_button = $(this);
            photo_button.html('<?php echo __('panel/panel.wait'); ?>');
            var custom_tag = $(this).parent().parent().find('.custom_tag').val();
            $.ajax({
                type: 'post',
                url: '/instagram/save_custom_tag',
                dataType: 'json',
                data: {
                    tag: custom_tag
                },
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                },
                success: function (data) {
                    photo_button.html('<?php echo __('panel/panel.save'); ?>');
                    if (data != false) {
                        photo_button.parent().find('.custom_tag').val(data);
                        swal(
                            '',
                            '<?php echo __('panel/panel.regular'); ?>',
                            'success'
                        );
                    } else {
                        swal(
                            'Oops...',
                            '<?php echo __('panel/panel.error'); ?>',
                            'error'
                        )
                    }
                },
                error: function (data) {
                    alert('Error:', data);
                }
            })
        });
    }

</script>
@yield('extras')
</body>

</html>