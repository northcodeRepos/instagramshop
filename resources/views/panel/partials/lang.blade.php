<div class="form-group col-xs-12">
    <label class="col-md-4 control-label"><?php echo __('panel/panel.lang'); ?></label>
    <div class="col-md-8">
        <div class="col-xs-12 no_padding">
            <div class="col-xs-6 col-md-1 no_padding" style="margin-top: 6px;">Panel</div>
            <div class="col-xs-6 col-md-11">
                <select name="setlocale_back" class="btn-group bootstrap-select bs-select form-control setLocale" lang_type="back">
                    <option value="def" @if(!Auth::user()->backend_locale) selected @endif><?php echo __('panel/panel.default'); ?></option>
                    <option value="en" @if(Auth::user()->backend_locale == 'en') selected @endif>English</option>
                    <option value="pl" @if(Auth::user()->backend_locale == 'pl') selected @endif>Polski</option>
                </select>

            </div>
        </div>

        <div class="col-xs-12 no_padding">
            <br>
            <div class="col-xs-6 col-md-1 no_padding"
                 style="margin-top: 6px;"><?php echo __('panel/panel.profile'); ?></div>
            <div class="col-xs-6 col-md-11">
                <select name="setlocale_front" @if(Auth::check() && !isset(Auth::user()->instagram->insta_id)) disabled @endif class="btn-group bootstrap-select bs-select form-control setLocale" lang_type="front">
                    <option value="def" @if(!Auth::user()->frontend_locale) selected @endif><?php echo __('panel/panel.default'); ?></option>
                    <option value="en" @if(Auth::user()->frontend_locale == 'en') selected @endif>English</option>
                    <option value="pl" @if(Auth::user()->frontend_locale == 'pl') selected @endif>Polski</option>
                </select>

            </div>
        </div>

    </div>

    <script>

    </script>

</div>