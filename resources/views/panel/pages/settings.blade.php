@extends('panel.index')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light tasks-widget bordered">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if(Auth::check() && !isset(Auth::user()->instagram->insta_id))
                    @include('panel.partials.connect')
                    <hr>
                    <div class="row">
                        @include('panel.partials.lang')
                    </div>
                @else
                    <p><?php echo __('panel/panel.photos_in'); ?> : <b>{{Auth::user()->instagrammedia->count()}}</b></p>
                    <p>
                        <?php echo __('panel/panel.down_below-edit'); ?>
                    </p>
                    <hr>

                    <div class="row">
                        <div class="col-sm-12 insta_photo" id="media_row-id">

                            <div class="col-sm-12 col-md-10 ">

                                <div class="form-group col-xs-12">
                                    <label class="col-md-4 control-label"><?php echo __('panel/panel.auto_link'); ?></label>
                                    <div class="col-md-8">
                                        @if(\App\Helpers\Helper::getAutoLink() == NULL)
                                            <button class="btn btn-success auto_link">
                                                <?php echo __('panel/panel.on'); ?>
                                            </button>
                                        @else
                                            <button class="btn btn-danger auto_link">
                                                <?php echo __('panel/panel.off'); ?>
                                            </button>
                                            @endif
                                            </span>
                                    </div>

                                </div>
                                <div class="form-group col-xs-12">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="col-md-8">
                                        <input @if(\App\Helpers\Helper::getAutoLink() == null) readonly
                                               @endif type="text" style="width: 400px !important;max-width: 60%;"
                                               class="form-control input-inline input-large auto_link_url"
                                               placeholder="http://example.com/instagram_api.php"
                                               value="{{\App\Helpers\Helper::getAutoLinkUrl()}}">
                                        <span class="help-inline"> <button
                                                    @if(\App\Helpers\Helper::getAutoLink() == null) disabled
                                                    @endif class="btn btn-success save_auto_link_url"><?php echo __('panel/panel.save'); ?></button> </span>

                                    </div>

                                </div>
                                <!--
                                <div class="form-group col-xs-12">
                                    <label class="col-md-4 control-label">Match TAG</label>
                                    <div class="col-md-8">
                                        <input @if(\App\Helpers\Helper::getMatchTag() == null) style="display: none;"
                                               @endif type="text"
                                               class="form-control input-inline input-large custom_tag"
                                               placeholder="[a-zA-Z0-9\-\_]+"
                                               @if(\App\Helpers\Helper::getMatchTag() != null) value="{{\App\Helpers\Helper::getMatchTag()}}" @endif>
                                        <button @if(\App\Helpers\Helper::getMatchTag() == null) style="display: none;"
                                                @endif class="btn btn-success save_custom_tag">
                                            <?php echo __('panel/panel.save'); ?>
                                        </button>
                                        @if(\App\Helpers\Helper::getMatchTag() == NULL)
                                            <button
                                                    class="btn btn-success match_tag">
                                                <?php echo __('panel/panel.on'); ?>
                                            </button>
                                        @else
                                            <button
                                                    class="btn btn-danger match_tag">
                                                <?php echo __('panel/panel.off'); ?>
                                            </button>

                                            @endif
                                            </span>
                                    </div>

                                </div> -->
                                @include('panel.partials.lang')
                            </div>
                        </div>
                    </div>

                @endif

            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop

@section('extras')
    <script>
        $(window).load(function() {
            $('select').on('change', function() {
                $.get('/panel/setlocale/' + $(this).attr('lang_type') +'/'+ this.value , function (data) {
                    if (data == 'true') {
                        location.reload();
                    } else {
                        swal(
                            'Oops...',
                            '<?php echo __('panel/panel.error'); ?>',
                            'error'
                        )
                    }
                });
            });
        });

    </script>
@stop
