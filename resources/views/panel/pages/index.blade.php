@extends('panel.index')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light tasks-widget bordered">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if(!Auth::user()->instagram()->first())
                    @include('panel.partials.connect')
                @else
                    <h3><?php echo __('global.welcome'); ?> {{Auth::user()->instagram->full_name}}</h3>
                    <p><?php echo __('panel/panel.photos_in'); ?> : <b>{{Auth::user()->instagrammedia->count()}}</b></p>

                    @if($private != null)
                        <div class="alert alert-info">
                            <?php echo __('panel/panel.info-private'); ?>
                        </div>
                    @endif
                    <div class="row">
                        @foreach(Auth::user()->instagrammedia as $media)
                            @if($media->hidden == 1)
                                <div class="col-sm-12 col-md-2 insta_photo">
                                    <div class="stats">
                                        <i class="icon-link"></i>
                                        <b>{{$media->Stats->where('instagram_media_id', $media->id)->count()}}</b>
                                    </div>
                                    <img width="100%" style="opacity:0.1;" src="{{urldecode($media->image)}}" alt="">
                                </div>
                            @else
                                <div class="col-sm-12 col-md-2 insta_photo">
                                    <div class="stats">
                                        <i class="icon-link"></i>
                                        <b>{{$media->Stats->where('instagram_media_id', $media->id)->count()}}</b>
                                    </div>

                                    <a target="_blank" media_id="{{$media->id}}"
                                       href="@if($media->custom_link){{ \App\Helpers\Helper::getMediaURL($media->id) }} @else {{$media->link}} @endif">
                                        <img width="100%" src="{{urldecode($media->image)}}" alt="">
                                    </a>
                                </div>
                            @endif

                        @endforeach
                    </div>
                    <button type="button" class="btn btn-sm green-meadow" id="refresmedia"><?php echo __('panel/panel.refresh_photos'); ?></button>
                    <button type="button" class="btn btn-sm red pull-right" id="unconnect"><?php echo __('panel/panel.disconnect'); ?>
                    </button>
                @endif

            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop
