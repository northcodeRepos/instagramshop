@extends('panel.index')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light tasks-widget bordered">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if(Auth::check() && !isset(Auth::user()->instagram->insta_id))
                    @include('panel.partials.connect')
                @else
                    <p><?php echo __('panel/panel.photos_in'); ?> : <b>{{Auth::user()->instagrammedia->count()}}</b></p>
                    <p>
                        <?php echo __('panel/panel.down_below'); ?>
                    </p>
                    <hr>

                    @foreach(Auth::user()->instagrammedia()->get() as $media)
                        <div class="row">
                            <div class="col-sm-12 insta_photo" id="media_row-id_{{$media->media_id}}">
                                <div class="col-sm-12 col-md-2 no_padding"><img width="100%"
                                                                                src="{{urldecode($media->image)}}"
                                                                                alt=""></div>
                                <div class="col-sm-12 col-md-10 ">
                                    <div class="form-group  col-xs-12">
                                        <label class="col-md-4 control-label">ID :</label>
                                        <div class="col-md-8">
                                            <b>#{{$media->media_id}}</b>
                                        </div>
                                    </div>
                                    <div class="form-group  col-xs-12">
                                        <label class="col-md-4 control-label"><?php echo __('panel/panel.org_link'); ?> :</label>
                                        <div class="col-md-8">
                                            <b><a target="_blank" href="{{$media->link}}">{{$media->link}}</a></b>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label class="col-md-4 control-label"><?php echo __('panel/panel.custom_link'); ?> :</label>
                                        <div class="col-md-8">
                                            <input @if(\App\Helpers\Helper::getAutoLink() != null) readonly @endif type="text" style="width: 400px !important;max-width: 60%;"
                                                   class="form-control input-inline input-large custom_url"
                                                   placeholder="http://babyliss-totallook.pl/product/D302RE"
                                                   value="{{$media->custom_link}}">
                                            <span class="help-inline"> <button @if(\App\Helpers\Helper::getAutoLink() != null) disabled @endif media_id="{{$media->media_id}}"
                                                                               class="btn btn-success save_custom_url"><?php echo __('panel/panel.save'); ?></button> </span>

                                        </div>

                                    </div>

                                    <div class="form-group col-xs-12">

                                        <div class="col-md-4">
                                            <button media_id="{{$media->media_id}}"
                                                    class="btn btn-success show_hide_photo"><?php echo __('panel/panel.show_hide'); ?>
                                            </button>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="pull-left"><?php echo __('panel/panel.photo_is'); ?> : @if($media->hidden != 1) <span
                                                        style="color:green;" class="photo_status"><?php echo __('panel/panel.visible'); ?></span> @else
                                                    <span style="color: red;"
                                                          class="photo_status"><?php echo __('panel/panel.invisible'); ?></span> @endif</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @endforeach

                    <button type="button" class="btn btn-sm green-meadow" id="refresmedia"><?php echo __('panel/panel.refresh_photos'); ?></button>
                @endif

            </div>
            <!-- END PORTLET-->
        </div>
    </div>
@stop
