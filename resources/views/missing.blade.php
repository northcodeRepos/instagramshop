<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404</title>
    <style>
        img {
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            right: 0;
            margin:auto;
            max-width: 90%;
        }
    </style>
</head>
<body>
<img src="{{asset('images/404.png')}}" alt="">
</body>
</html>