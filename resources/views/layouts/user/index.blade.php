<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sklep Instagram') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="icon" href="{{asset('images/thumb.png')}}">
    <link href="{{asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.2/sweetalert2.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
</head>
<body>

<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="col-xs-12 col-md-1"></div>
            <div class="col-xs-12 col-md-10 ">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{asset('images/logo.png')}}" alt="">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <div class="search_box">
                        <div class="_etslc _1rn91"><a href="https://www.instagram.com"><span
                                        class="_oqxv9 coreSpriteSearchIcon"></span><span
                                        class="search_text"><?php echo __('profile/profile.search'); ?></span></a></div>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="https://www.instagram.com"
                               class="download_box"><?php echo __('profile/profile.download'); ?></a></li>
                        <li><a href="https://www.instagram.com/accounts/login/"
                               class="login_button"><?php echo __('auth.login'); ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-md-1"></div>
        </div>
    </nav>
    <div class="container">

        @yield('content')
    </div>

</div>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.2/sweetalert2.min.js"></script>
<style type="text/css">
    .profile_gall .insta_box_wrap.margin {
        margin-bottom: 250px;
    }

    .profile_gall .insta_box_wrap {
        position: static;
        -webkit-transition: all 0.2s ease;
        -moz-transition: all 0.2s ease;
        -o-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
</style>
<script>

    $(window).resize(function () {
        $('.prodcuts_wrap').hide();
        $('.insta_box_wrap').removeClass('margin');
        $('.insta_box_wrap').find('.prodcuts_wrap').removeClass('activated');
    });

    $('.insta_photo_wrap').on('click', function () {
        var element = $(this).parent();
        var cango = true;
        var button = element.find('.insta_photo');

        if (button.attr('link-disabled') === 'true') {
//
            if (element.hasClass('margin')) {
                $('.prodcuts_wrap').hide();
                $('.insta_box_wrap').removeClass('margin');
                if (element.find('.prodcuts_wrap').hasClass('activated')) {
                    $('.prodcuts_wrap').removeClass('activated');
                    cango = false;
                }
            }
//
            if(cango == true) {
                $('.insta_box_wrap').removeClass('margin');
                $('.prodcuts_wrap').removeClass('activated').hide();

                setTimeout(function () {
                    var elementPos = element.attr('id');
                    elementPos = $('#' + elementPos).position().top;
                    if (element.parent().find('.prodcuts_wrap').length) {
                        var toppos = parseInt(elementPos) - 30 + parseInt($('.insta_photo_wrap:not(.margin)').height());
//
                        element.find('.prodcuts_wrap').attr('style', 'top:' + toppos + 'px ;').addClass('activated').fadeIn('slow');
                        $.each($('.insta_box_wrap'), function (key, value) {
                            var Pos = $(value).attr('id');
                            Pos = $('#' + Pos).position().top;
                            if (Pos === elementPos) $(value).addClass('margin');
                        });
                    }
                }, 200);
                countClick(button, false);
            }

        } else {
            countClick(button, true);
        }

    });

    function countClick(button, linkout) {
        $.get('/profile/save_click/' + button.attr('data-media_id'), function (data) {
            var target = button.attr('data-target');
            if (linkout == true) {
                if (target === '_blank') {
                    window.open(url, target);
                } else {
                    window.location = target;
                }
            }
        });
    }
    $.each($('.products_many'), function (key, value) {
        var carousel = $(this);
        var this_carousel = $(this).owlCarousel({
            loop: true,
            autoHeight: true,
            nav: true,
            margin: 0,
            autoplay: false,
            responsive: {
                0: {
                    items: 1
                }
            }
        });

        carousel.parent().find('.products_left').on('click', function () {
            carousel.find('.owl-prev').trigger('click');
        })
        carousel.parent().find('.products_right').on('click', function () {
            carousel.find('.owl-next').trigger('click');
        })
    });

</script>

</body>
</html>
