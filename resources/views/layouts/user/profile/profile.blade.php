@extends('layouts.user.index')

@section('content')
    <div class="col-xs-12 col-md-1"></div>
    <div class="col-xs-12 col-md-10 no_padding">

        <div class="col-xs-12  profile_content">
            <div class="col-xs-3 col-sm-2 text-center col-md-4 profile_photo_box">
                <img class="img-circle profile_pic" style="max-width: 100%;" src="{{$userdata['user_avatar']}}" alt="">
            </div>
            <div class="col-xs-9 col-md-8 profile_desc">
                <h2><span>{{$userdata['username']}}</span> <a href="https://www.instagram.com/{{$userdata['username']}}"
                                                              target="_blank"
                                                              class="ob_button"><?php echo __('profile/profile.follow'); ?></a>
                </h2>
                <p class="hidden-xs">
                    <span><?php echo __('profile/profile.posts'); ?>: <b>{{$usermedia->count()}}</b></span>
                    <span><b>{{$userdata['followers']}}</b> <?php echo __('profile/profile.followers'); ?></span>
                    <span><?php echo __('profile/profile.followed'); ?>: <b>{{$userdata['following']}}</b></span>
                </p>
                <div class="col-xs-12 no_padding hidden-xs">
                    <a class="ext_url"
                       href="http://{{$userdata['user_url']}}"><b>{{$userdata['user_fullname']}}</b> {{$userdata['user_url']}}
                    </a>
                </div>

            </div>
            <div class="col-xs-12 no_padding hidden-sm hidden-lg hidden-md bottom_link">
                @if($userdata['user_url'])
                    <a class="ext_url"
                       href="http://{{$userdata['user_url']}}"><b>{{$userdata['user_fullname']}}</b> {{$userdata['user_url']}}
                    </a>
                @else
                    <span class="ext_url"><b>{{$userdata['user_fullname']}}</b>
                    </span>
                @endif

            </div>
            <div class="col-xs-12 bottom_info no_padding hidden-sm hidden-lg hidden-md">
                <span><?php echo __('profile/profile.posts'); ?>: <br><b>{{$usermedia->count()}}</b></span>
                <span><b>{{$userdata['followers']}}</b><br> <?php echo __('profile/profile.followers'); ?></span>
                <span><?php echo __('profile/profile.followed'); ?>:<br> <b>{{$userdata['following']}}</b></span>
            </div>
        </div>


        <div class="col-xs-12 no_padding profile_gall">
            @foreach($usermedia as $media)

                @if($media->hidden != 1)
                    <?php
                    $goToTag = null;
                    $productsCount = \App\Helpers\Helper::getTagsCount($media->id);
                    $getProducts = \App\Helpers\Helper::getProductsDetails($media->hashtags);
                    if(count($getProducts) != 0 && $productsCount) {
                        $prCnt = count($getProducts);
                    } else {
                        $prCnt = false;
                    }
                    ?>
                    <div class="insta_box_wrap col-xs-12 col-sm-6 col-md-4" id="media_{{$media->id}}">
                        <div class="insta_photo_wrap col-xs-12 no_padding">
                            <div class=" insta_photo"
                                 @if($prCnt != 0 && $getProducts) link-disabled="true"
                                 @else link-disabled="false" @endif data-media_id="{{$media->id}}"
                                 data-target="@if($media->custom_link){{ \App\Helpers\Helper::getMediaURL($media->id) }} @else {{$media->link}} @endif">
                                <img width="100%" src="{{urldecode($media->image)}}" alt="">
                            </div>
                            @if($prCnt && $getProducts)
                                <div class="hashtags_count">
                                    <span>{{$prCnt}} @if($prCnt > 1) <?php echo __('profile/profile.products'); ?> @else <?php echo __('profile/profile.product'); ?> @endif</span>
                                </div>

                            @endif
                        </div>
                        @if($prCnt !=0 && $getProducts)

                            <div class="prodcuts_wrap" >
                                @if($prCnt > 1)
                                    <div class="products_left"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                                    <div class="products_right"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                @endif

                                <div class="products @if($prCnt) products_many owl-theme owl-carousel @endif">

                                    @foreach($getProducts as $product)

                                        <div class="col-xs-12 product">
                                            <div class="col-xs-12 no_padding col-sm-8">
                                                <div class="product_image"><img src="{{$product->image_url}}" alt=""></div>
                                                <div class="product_desc">
                                                    <b class="product_name"> {{$product->name}}
                                                        @if(isset($product->model))<small>{{$product->model}}</small>@endif
                                                    </b>
                                                    @if(isset($product->desc))<p>{{$product->desc}}</p>@endif
                                                    <hr>
                                                    <b class="product_price">{{$product->price}} zł</b>
                                                </div>
                                                <div class="col-xs-12 text-center">
                                                    <a href="{{$product->product_url}}" class="buy_now" target="_blank"><i
                                                                class="fa fa-shopping-cart"
                                                                aria-hidden="true"></i><?php echo __('profile/profile.buy-now'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>


                        @endif
                    </div>

                @endif
            @endforeach
        </div>
        <div class="col-xs-12 col-md-1"></div>


@stop