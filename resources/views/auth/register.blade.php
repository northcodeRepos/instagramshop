@extends('auth.app.app')

@section('content')
    <div class="login-content">
        <h1><?php echo __('auth.register'); ?></h1>
        <form class="login-form" role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-xs-6{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"
                           autocomplete="off" placeholder="<?php echo __('auth.name'); ?>" name="name" value="{{ old('name') }}" required
                           autofocus/>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-xs-6{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="email"
                           autocomplete="off" placeholder="<?php echo __('auth.email'); ?>" name="email" value="{{ old('email') }}"
                           required/>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>
            <div class="row">
                <div class="col-xs-6{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password"
                           autocomplete="off" placeholder="<?php echo __('passwords.password_'); ?>" name="password" required/>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-xs-6">
                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password"
                           autocomplete="off" placeholder="<?php echo __('passwords.password_re'); ?>" name="password_confirmation" required/>
                </div>

            </div>
            <div class="col-sm-24 text-right">
                <div class="forgot-password">
                    <a href="{{ route('login') }}" class="forget-password"><?php echo __('auth.login'); ?></a>
                </div>
                <button class="btn blue" type="submit"><?php echo __('auth.register_'); ?></button>
            </div>

        </form>
    </div>
@stop