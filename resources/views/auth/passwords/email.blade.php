@extends('auth.app.app')

@section('content')

    <div class="login-content">

        <form class="forget-form" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <h3 class="font-green"><?php echo __('passwords.forgot_'); ?></h3>
            <p> <?php echo __('passwords.put_email'); ?> </p>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off"
                       value="{{ old('email') }}" required placeholder="<?php echo __('auth.email'); ?>" name="email">
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-actions">
                <a href="{{route('login')}}" type="button" id="back-btn"
                   class="btn green btn-outline"><?php echo __('global.back'); ?></a>
                <button type="submit" class="btn btn-success uppercase pull-right"><?php echo __('passwords.reset_'); ?></button>
            </div>

        </form>
    </div>

@stop