@extends('auth.app.app')

@section('content')

    <div class="login-content">
        <h1><?php echo __('passwords.reset_'); ?></h1>

        <form class="login-form" role="form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <input type="hidden" name="token" value="{{ $token }}">


            <div class="row">
                <div class="col-xs-12{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group "
                           type="email"
                           autocomplete="off" placeholder="<?php echo __('auth.email'); ?>" name="email"
                           value="{{ $email or old('email') }}" required autofocus/>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>


            </div>
            <div class="row">
                <div class="col-xs-6{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group"
                           type="password"
                           autocomplete="off" placeholder="<?php echo __('passwords.password_'); ?>" name="password" required/>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="col-xs-6{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group"
                           type="password"
                           autocomplete="off" placeholder="<?php echo __('passwords.password_re'); ?>" name="password_confirmation"
                           required/>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12 text-right">
                    <div class="forgot-password">
                        <a href="{{ route('login') }}" class="forget-password"><?php echo __('auth.login'); ?> &nbsp;</a>
                    </div>
                    <button class="btn blue" type="submit"><?php echo __('passwords.reset_'); ?></button>
                </div>
            </div>
        </form>
    </div>

@stop
