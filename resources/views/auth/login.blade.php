@extends('auth.app.app')

@section('content')
    <div class="login-content">
        <h1><?php echo __('auth.login_'); ?></h1>
        <form class="login-form" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row">
                <div class="col-xs-6{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group " type="email"
                           autocomplete="off" placeholder="<?php echo __('auth.email'); ?>" name="email" autofocus
                           value="{{ old('email') }}" required/>
                    @if ($errors->has('email'))
                        <span class="help-block" style="    margin-bottom: 20px;">
                                        <strong>{{ $errors->first('email') }}<br></strong>

                                    </span>
                    @endif
                </div>

                <div class="col-xs-6{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password"
                           autocomplete="off" placeholder="<?php echo __('passwords.password_'); ?>" name="password" required/>
                    @if ($errors->has('password'))
                        <span class="help-block" style="    margin-bottom: 20px;">
                                        <strong>{{ $errors->first('password') }}<br></strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} value="1"/>
                        <?php echo __('auth.remember'); ?>
                        <span></span>
                    </label>
                </div>
                <div class="col-sm-8 text-right">
                    <div class="forgot-password">
                        <a href="{{ route('password.request') }}" id="forget-password" class="forget-password"><?php echo __('auth.forgot'); ?> &nbsp;</a>
                        <a href="{{ route('register') }}" id="forget-password" class="forget-password"><?php echo __('auth.reg'); ?></a>
                    </div>
                    <button class="btn blue" type="submit"><?php echo __('auth.login'); ?></button>
                </div>
            </div>

        </form>
    </div>
@stop