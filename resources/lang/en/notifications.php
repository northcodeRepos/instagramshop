<?php

// resources/lang/en/notifications.php

return [
    'regrats' => 'Best Regards',
    'problem_01' => 'If you have a problem with the button click',
    'problem_02' => 'Copy and paste or click the link below',
    'click' => 'Click below to activate your account.',
    'activate' => 'Activate account',

];