<?php

// resources/lang/en/panel/panel.php

return [
    'photos_in' => 'Photos on the site',
    'views' => 'Profile Visits',
    'down_below' => 'Below you can edit custom links and hide pictures.',
    'down_below-edit' => 'Below you can decide if the service will automatically link your photos.',
    'wait' => 'Please wait...',
    'error' => 'An error occured.',
    'lang' => 'Choose language',
    'profile' => 'Profile',
    'default' => 'Default',

    //

    'refresh_photos' => 'Refresh list of photos',
    'disconnect' => 'Disconnect the instagram account',
    'full' => 'To take full advantage of your account',
    'connect' => 'Connect your instagram account',



    //Edycja
    'show_hide' => 'Hide / Show photo',
    'save' => 'Save',
    'visible' => 'Visible',
    'invisible' => 'Invisible',
    'org_link' => 'Original link',
    'custom_link' => 'Custom link',
    'photo_is' => 'Photo is',
    'link' => 'You need to provide a link!',
    'link-saved' => 'Link has been saved!',
    'auto_mode' => 'You are in auto link mode, you can not use this option.',


    //Ustawienia

    'auto_link' => 'Automatic linking',
    'on' => 'Turn on',
    'off' => 'Turn off',
    'regular' => 'Regular expression was saved!',


    //info
    'info-private' => 'Your instagram profile is set to private, the data may not be up to date.',
    'info-private-refresh' => 'Failed to retrieve current photo list, your profile is private.',


    //Insta connect

    'cannot_connect_01' => 'Failed, the given Instagram account is linked to another account.',
    'cannot_connect_02' => 'Failed to connect to instagram account.',
    'connected' => 'Success ! You have linked your account to the instagram profile!',


    // Api

    'class_not_found' => 'Given url are not compatible with our system.'
];