<?php

// resources/lang/en/profile/profile.php

return [
    //profile.blade.php
    'follow' => 'Follow',
    'posts' => 'Posts',
    'followers' => 'followers',
    'followed' => 'Followed',
    'buy-now'=> 'Buy now',
    'product'=> 'Product',
    'products'=> 'Products',

    //Index.blade.php
    'search' => 'Search',
    'download' => 'Download app',
];