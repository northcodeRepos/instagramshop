<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Login credentials are not correct. Make sure you have activated your account.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds',
    'remember' => 'Remember me',
    'forgot' => 'Forgot your password?',
    'reg' => 'Register',
    'email' => 'E-mail adress',
    'login_' => 'Log in',
    'register' => 'Registration',
    'name' => 'Name',
    'register_' => 'Register',
    'login' => 'Sign In',
    'logout' => 'Log out',


    'registered' => 'Account created. To log in, activate your account via the email we sent you.',
    'activated' => 'Account has been activated. You can now log in.',
    'verify' => 'Account verification',

];
