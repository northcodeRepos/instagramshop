<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must have at least six characters and agree with the confirmation.',
    'reset' => 'Your password has been reset!',
    'reset_' => 'Reset password',
    'sent' => "We\'ve sent an email with a password reset link!",
    'token' => 'This password reset token is invalid.',
    'user' => "We can not find a user with this email address.",
    'password_' => "Password",
    'password_re' => "Repeat password",
    'forgot_' => 'Forgotten password ?',
    'put_email' => 'Please enter your email address to reset it.',
    'you_recive' => 'You are receiving this email because we received a password reset request for your account',
    'if_not' => 'If you did not request a password reset, you do not need to take any action.',
    'pw_res' => 'Reset password',

];
