<?php

// resources/lang/pl/notifications.php

return [
    'regrats' => 'Pozdrawiamy',
    'problem_01' => 'Jeśli masz problem z kliknięciem w button',
    'problem_02' => 'skopiuj i wklej lub kliknij poniższy link',
    'click' => 'Kliknij poniżej aby aktywować swoje konto.',
    'activate' => 'Aktywuj konto',

];