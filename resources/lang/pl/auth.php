<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Dane logowania są nie poprawne. Upewnij się, że aktywowałeś/łaś konto.',
    'throttle' => 'Zbyt wiele prób logowania. Proszę spróbować ponownie za :seconds seconds.',
    'remember' => 'Zapamiętaj mnie',
    'forgot' => 'Zapomniałeś hasło?',
    'reg' => 'Zarejestruj się',
    'email' => 'Adres email',
    'login_' => 'Logowanie',
    'register' => 'Rejestracja',
    'name' => 'Nazwa',
    'register_' => 'Zarejestruj',
    'login' => 'Zaloguj się',
    'logout' => 'Wyloguj',

    'registered' => 'Konto zostało założone. Aby się zalogować aktywuj konto przez wiadomość email którą do ciebie wysłaliśmy',
    'activated' => 'Konto zostało aktywowane. Możesz się teraz zalogować.',
    'verify' => 'Weryfikacja konta',

];
