<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasła muszą mieć co najmniej sześć znaków i zgadzać się z potwierdzeniem.',
    'reset' => 'Twoje hasło zostało zresetowane!',
    'reset_' => 'Zresetuj hasło',
    'sent' => 'Wysłaliśmy e-maila z linkiem resetowania hasła!',
    'token' => 'Ten token resetowania hasła jest nieprawidłowy.',
    'user' => "Nie możemy znaleźć użytkownika z tym adresem e-mail.",
    'password_' => "Hasło",
    'password_re' => "Powtórz hasło",
    'forgot_' => 'Zapomniane hasło ?',
    'put_email' => 'Podaj swój adres email aby je zresetować.',
    'you_recive' => 'Otrzymujesz ten e-mail, ponieważ otrzymaliśmy prośbę o resetowanie hasła dla Twojego konta',
    'if_not' => 'Jeśli nie zażądałeś zresetowania hasła, nie musisz podejmować żadnych działań.',
    'pw_res' => 'Reset hasła',

];
