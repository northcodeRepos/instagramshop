<?php

// resources/lang/pl/panel.php

return [
    'photos_in' => 'Zdjęć w serwisie',
    'views' => 'Wizyt profilu',
    'down_below' => 'Poniżej możesz edytować niestandardowe linki oraz chować zdjęcia.',
    'down_below-edit' => 'Poniżej możesz zdecydować czy serwis ma automatycznie dobierać linki twoich zdjęć.',
    'wait' => 'Proszę czekać...',
    'error' => 'Wystąpił błąd.',
    'lang' => 'Wybierz język',
    'profile' => 'Profil',
    'default' => 'Domyślny',

    //

    'refresh_photos' => 'Odśwież listę zdjęć',
    'disconnect' => 'Odłącz konto instagram',
    'full' => 'Aby w pełni kożystać z konta',
    'connect' => 'Połącz konto z instagramem',



    //Edycja
    'show_hide' => 'Schowaj/Pokaż zdjęcie',
    'save' => 'Zapisz',
    'visible' => 'Widoczne',
    'invisible' => 'Niewidoczne',
    'org_link' => 'Oryginalny link',
    'custom_link' => 'Niestandardowy link',
    'photo_is' => 'Zdjęcie jest',
    'link' => 'Musisz podać link!',
    'link-saved' => 'Link został zapisany!',
    'auto_mode' => 'Jesteś w trybie automatycznego linkowania, nie możesz kożystać z tej opcjii.',


    //Ustawienia

    'auto_link' => 'Automatyczne linkowanie',
    'on' => 'Włącz',
    'off' => 'Wyłącz',
    'regular' => 'Wyrażenie regularne zostało zapisane!',

    //info
    'info-private' => 'Twój profil instagram jest ustawiony na prywatny, dane mogą być nie aktualne.',
    'info-private-refresh' => 'Nie udało się pobrać aktualnej listy zdjęć, twój profil jest prywatny.',


    //Insta connect
    'cannot_connect_01' => 'Nie udało się, podane konto Instagram jest powiązane z innym kontem.',
    'cannot_connect_02' => 'Nie udało się połączyć konta z instagram-em.',
    'connected' => 'Sukces ! Połączyłeś swoje konto z profilem instagram!',


    //Api

    'class_not_found' => 'Nie znaleziono integracji z podanym adresem url'
];