<?php

// resources/lang/pl/profile.php

return [
    //profile.blade.php
    'follow' => 'Obserwuj',
    'posts' => 'Posty',
    'followers' => 'obserwujących',
    'followed' => 'Obserwowani',
    'buy-now'=> 'Kup Teraz',
    'product'=> 'Produkt',
    'products'=> 'Produkty',

    //Index.blade.php
    'search' => 'Szukaj',
    'download' => 'Pobierz aplikację',
];