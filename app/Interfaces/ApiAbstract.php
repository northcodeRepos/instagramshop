<?php

namespace App\Interfaces;

abstract class ApiAbstract
{
    abstract public function getProducts($products);
    abstract protected function configuration();
}