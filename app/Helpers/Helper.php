<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Model\InstagramMedia as Media;
use App\Model\Settings;
use App\Repositories\ApiRepository;

class Helper
{

//    public static function giveMeTagUrl($media_id)
//    {
//        $media = Media::find($media_id);
//        $link = self::getAutoLinkUrl();
//        $media_tags = $media->hashtags;
//        $tags = explode(',', $media_tags);
//        $match_tag = Settings::first()->match_tag;
//
//        if ($link) $id = self::separateProductID($link, '/');
//        if ($link && $id) $link = self::cutProductIDFromURL($link, $id);
//
//        $givenURL = '' . $link . '' . $tags[0] . '';
//
//        if ($match_tag && $media_tags && $link) {
//            $tags_limited = self::limitTags($match_tag, $tags);
//            dd($tags_limited);
//            if ($tags_limited[0]) $givenURL = '' . $link . '' . $tags_limited[0] . '';
//        }
//
//        //Response
//        if ($match_tag && $media_tags && $link) return json_encode($givenURL);
//        if (!$match_tag && $media_tags && $link) return json_encode($givenURL);
//
//    }

    public static function getTagsCount($media_id)
    {
        $media = Media::find($media_id);
        $media_tags = $media->hashtags;
        if (!$media_tags) {
            return json_encode(0);
        } else {
            $media_tags = explode(',', $media_tags);
            return json_encode(sizeof($media_tags));
        }

    }

    public static function getProductsDetails($hashTags)
    {

        $linkAuto = self::getAutoLinkUrl();
        if (file_exists(app_path("Classes/" . ApiRepository::findApiClass($linkAuto) . ".php"))) {
            $className = ApiRepository::findApiClass($linkAuto);
            $className = "App\\Classes\\$className";
            $api = new $className;
            $products = $api->getProducts([$hashTags]);
            if ($products) return $api->getProducts([$hashTags]);
        } else {
            return false;
        }


    }

    public static function getAutoLink()
    {
        $autolink = Settings::first()->auto_link;
        return $autolink;
    }

    public static function getAutoLinkUrl()
    {
        $auto_link_url = Settings::first()->auto_link_url;
        return $auto_link_url;
    }

    public static function getMatchTag()
    {
        $match = Settings::first()->match_tag;
        return $match;
    }

    public static function getMediaURL($media_id)
    {
        $findMedia = Media::find($media_id);
        if (\App\Helpers\Helper::getAutoLink() == null) {
            if($findMedia->custom_link) return $findMedia->custom_link;
            if($findMedia->link) return $findMedia->link;
        }
    }

    public static function limitTags($limiter, $tags)
    {
        return preg_grep("/" . $limiter . "/i", $tags);
    }

    public static function separateProductID($link, $separator)
    {
        return substr($link, strrpos($link, $separator) + 1);
    }

    public static function cutProductIDFromURL($link, $id)
    {
        return str_replace($id, "", $link);
    }
}