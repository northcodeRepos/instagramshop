<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Faker;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        return redirect('/panel/');
    }

    public function fakeapi($products, Request $request)
    {
        $products = explode(',', $products);
        $callBackProducts = collect(Array());
        $faker = Faker\Factory::create();
        $productsCount = 0;
        foreach ($products as $product) {
                $backProduct = [];
                $backProduct['name'] = $faker->word;
                $backProduct['model'] = $product;
                $backProduct['desc'] = $faker->text;
                $backProduct['price'] = $faker->randomNumber(2);
                $backProduct['photo'] = "http://lorempixel.com/700/800/cats/";
                $backProduct['product-link'] = "http://babyliss-totallook.pl/product/ST389E";
                $callBackProducts->push($backProduct);

        }

        return json_encode($callBackProducts);
    }

    public function logout()
    {
        $this->middleware('auth');
        Auth::logout();
        return redirect(route('home'));
    }
}
