<?php

namespace App\Http\Controllers;

use App\Model\User;

use Illuminate\Http\Request;
use MetzWeb\Instagram\Instagram;
use Auth;
use App\Model\InstagramMedia as Medias;
use Carbon\Carbon;
use App\Repositories\UserRepository;
use App\Repositories\MediaRepository;
use App\Model\Settings as Set;
use App\Repositories\ApiRepository;

class InstagramController extends Controller
{

    /**
     * @var \App\Repositories\UserRepository
     */
    protected $userRepository;
    protected $mediaRepository;


    public function __construct(UserRepository $userRepository, MediaRepository $mediaRepository, ApiRepository $apiRepository, Set $set)
    {

        $this->userRepository = $userRepository;
        $this->mediaRepository = $mediaRepository;
        $this->apiRepository = $apiRepository;
        $this->set = $set;

    }

    public function connect(Request $request)
    {
        return redirect(json_decode($this->userRepository->connectInstagram($request->user())));
    }

    public function unconnect(Request $request)
    {

        if (!$request->user()->instagram->insta_id)
            return json_encode(false);

        $this->userRepository->revokeInstagram($request->user());
        return json_encode(true);

    }


    public function toggleMedia($media_id)
    {
        $toggle = $this->mediaRepository->toggleMedia($media_id);
        return json_encode($toggle);

    }

    public function auto_link()
    {
        $toggle = $this->mediaRepository->auto_link();
        return json_encode($toggle);

    }

    public function match_tag()
    {
        $toggle = $this->mediaRepository->match_tag();
        return json_encode($toggle);

    }


    public function save_custom_url(Request $request)
    {
        $url = $request->get('url');
        $media_id = $request->get('media_id');

        if (\App\Helpers\Helper::getAutoLink() != null) return json_encode(__('panel/panel.auto_mode'));

        $this->mediaRepository->setCustomUrl($media_id, $url);

        return json_encode(true);
    }

    public function save_auto_link_url(Request $request)
    {
        $url = $request->get('url');
        $classPath = $this->apiRepository->findApiClass($url);
        if ($classPath) {
            $this->mediaRepository->save_auto_link_url($url);
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }

    public function save_custom_tag(Request $request)
    {
        $tag = $request->get('tag');
        if (!$tag) return json_encode(false);

        $this->set->save_custom_tag($tag);

        return json_encode($this->set->first()->match_tag);
    }

    public function refreshmedia(Request $request)
    {

        $now = Carbon::now();
        $countMedia = Medias::where('user_id', (Auth::user()->id))->count();
        if ($countMedia == 0) {
            $lastMediaTime = Carbon::now();
        } else if (Medias::where('user_id', (Auth::user()->id))->where('updated_at', '!=', NULL)->orderBy('updated_at', 'DESC')->first() == null) {
            $lastMediaTime = Carbon::now();
        } else {
            $lastMediaTime = Medias::where('user_id', (Auth::user()->id))->where('updated_at', '!=', NULL)->orderBy('updated_at', 'DESC')->first()->updated_at;
        }
        $diff = $lastMediaTime->diffInMinutes($now);
        if ($diff < 5) {
            $refresher = $this->userRepository->refreshInstagramMedia($request->user());
            if (json_decode($refresher) == 'private_profile') {
                return json_encode('private_profile');
            }
        };

        return json_encode(true);
    }

    public function login(Request $request)
    {

        $login = $this->userRepository->saveInstaProfile($request->user(), $request->get('code'));
        if (json_decode($login) == 'exist') return redirect('/panel')
            ->with('error', __('panel/panel.cannot_connect_01'));
        if (json_decode($login) == 'error') return redirect('/panel')
            ->with('error', __('panel/panel.cannot_connect_02'));
        if (json_decode($login) == 'true') return redirect('/panel')
            ->with('status', __('panel/panel.connected'));

    }
}
