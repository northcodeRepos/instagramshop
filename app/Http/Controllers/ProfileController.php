<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MediaRepository;
use App\Repositories\UserRepository;
use App\Repositories\StatisticsRepository;
use Auth;

class ProfileController extends Controller
{

    protected $mediaRepository;
    protected $userRepository;
    protected $statisticsRepository;

    public function __construct(MediaRepository $mediaRepository, UserRepository $userRepository, StatisticsRepository $statisticsRepository)
    {

        $this->media = $mediaRepository;
        $this->user = $userRepository;
        $this->stats = $statisticsRepository;

    }

    public function profile(Request $request)
    {

        $host = $request->getHost();

        $username = implode(array_slice(explode(".", $host), 0, 1), ".");
        $homedomain = str_replace('' . $username . '', '', $host);

        $userProfile = $this->user->findInstaProfile($username)->first();
        if (!$userProfile) return view('missing');

        $userProfileLang = $userProfile->first()->frontend_locale;
        if($userProfileLang != env('APP_LANG') && $userProfileLang) {
            \App::setLocale($userProfileLang);
        }

//        $refresher = $this->user->refreshInstagramMedia($userProfile);

        $userid = $userProfile->first()->id;
        $user = $this->user->find($userid);
        $usermedia = $user->instagrammedia()->get();

        $publicUserProfile = $this->user->findPublicProfile($userProfile);

        if(isset($publicUserProfile['external_url'])) {
            $user_url = str_replace(['http://', 'https://', '/'], '', $publicUserProfile['external_url']);
        } else {
            $user_url = null;
        }
        $userdata = collect(['user_fullname' => $publicUserProfile['full_name'], 'user_url' => $user_url, 'profileurl' => '' . $username . '' . $homedomain . '', 'user_avatar' => $publicUserProfile['profile_pic_url'], 'user_id' => $userProfile->id, 'username' => $username, 'photos' => $usermedia->count(), 'followers' => $publicUserProfile['followed_by']['count'], 'following' => $publicUserProfile['follows']['count']]);

        $this->stats->countVisit($request->user(), $username);

        return view('layouts.user.profile.profile', ['usermedia' => $usermedia, 'userdata' => $userdata->all(),'goToTag' => null]);


    }

    public function save_click($acc, $media_id, Request $request)
    {
        $this->stats->countClick($request->user(), $media_id, $acc);
        return json_encode(true);

    }
}
