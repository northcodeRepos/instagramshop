<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;
use Auth;
use App\Model\Statistics as Stats;
use App\Repositories\UserRepository as UserRepository;
use Illuminate\Support\Facades\Redirect;

class PanelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->user = $userRepository;
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function setBackendLocale()
    {
        $userBackendLang = Auth::user()->backend_locale;
        if ($userBackendLang != env('APP_LANG') && $userBackendLang) {
            \App::setLocale($userBackendLang);
        }
    }

    public function setlocale($type, $lang, Request $request)
    {
        $user = User::find($request->user()->id);
        if ($type && $type == 'back' && $lang && $lang != 'def') {
            $user->backend_locale = $lang;
            $user->save();
        } else if ($type && $type == 'front' && $lang && $lang != 'def') {
            $user->frontend_locale = $lang;
            $user->save();
        } else if($type  &&  $type == 'back' && $lang == 'def') {
            $user->backend_locale = null;
            $user->save();
        } else if($type  &&  $type == 'front' && $lang == 'def') {
            $user->frontend_locale = null;
            $user->save();
        }
        return json_encode(true);

    }

    public function index(Request $request)
    {
        $profile_visits = 0;
        $refresher = null;
        $this->setBackendLocale();

        if (isset($request->user()->instagram()->first()->username)) {
            $username = $request->user()->instagram()->first()->username;
            $userProfile = $this->user->findInstaProfile($username)->first();
//            $refresher = $this->user->refreshInstagramMedia($userProfile);
            $refresher = json_decode($refresher);
            if ($refresher == 'private_profile') {
                $refresher = true;
            }
            $profile_visits = Stats::where('visited', $username)->where('type', 1)->count();
        }

        return view('panel.pages.index', ['visits' => $profile_visits, 'private' => $refresher]);
    }

    public function edit()
    {
        $this->setBackendLocale();
        return view('panel.pages.edit');
    }

    public function settings()
    {
        $this->setBackendLocale();
        return view('panel.pages.settings');
    }
}
