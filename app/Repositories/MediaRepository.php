<?php

namespace App\Repositories;

use App\Model\InstagramMedia as Media;
use App\Model\Settings as Set;

class MediaRepository
{
    public function __construct(Media $media, Set $set)
    {
        $this->media = $media;
        $this->set = $set;
    }

    public function insertMedia($media)
    {

        $this->media->insertMedia($media);

    }

    public function findMedia($media_id)
    {
        $finder = $this->media->findMedia($media_id);
        return $finder->first();
    }

    public function updateMedia($media_id, $hashtags)
    {
        $media = $this->media->findMedia($media_id);
        $this->media->updateMedia($media, $hashtags);
    }

    public function toggleMedia($media_id)
    {

        $media = $this->media->findMedia($media_id);
        if (!$media->first()->hidden) {
            $this->hideMedia($media);
        } else {
            $this->showMedia($media);
        }
        return json_encode($media->first()->hidden);
    }

    public function auto_link()
    {
        if ($this->set->first()->auto_link == null) {
            $this->set->auto_link_on();
        } else {
            $this->set->auto_link_off();
        }
        return json_encode($this->set->first()->auto_link);
    }

    public function match_tag()
    {

        if ($this->set->first()->match_tag == null) {
            $this->set->match_tag_on();
        } else {
            $this->set->match_tag_off();
        }
        return json_encode($this->set->first()->match_tag);
    }

    public function save_custom_tag($media_id, $tag)
    {

        $this->media->save_custom_tag($this->findMedia($media_id), $tag);
    }

    public function setCustomUrl($media_id, $url)
    {
        $this->media->setCustomUrl($this->findMedia($media_id), $url);
    }

    public function save_auto_link_url($url)
    {
        $this->set->save_auto_link_url($url);
    }

    public function hideMedia($media)
    {
        $this->media->hideMedia($media);
    }

    public function showMedia($media)
    {
        $this->media->showMedia($media);
    }


    public function auto_link_on($media)
    {
        $this->media->auto_link_on($media);
    }

    public function auto_link_off($media)
    {
        $this->media->auto_link_off($media);
    }

    public function match_tag_on($media)
    {
        $this->media->match_tag_on($media);
    }

    public function match_tag_off($media)
    {
        $this->media->match_tag_off($media);
    }
}