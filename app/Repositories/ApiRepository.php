<?php

namespace App\Repositories;

use App\Helpers\Helper;
use App\Model\Settings as Set;


class ApiRepository
{
    public function __construct(Set $set)
    {
        $this->set = $set;
    }

    public static function findApiClass($string)
    {

        $enabledUrls = include config_path('apiEnabledUrls.php');
        $enabledUrls = collect($enabledUrls);
        foreach ($enabledUrls as $partner) {
            $partner = collect($partner);
            $partner_url = $partner->all()['url'];
            $givenUrl = str_replace(['http://', 'https://'], "", $string);
            if ($givenUrl == $partner_url) {
                if (file_exists(app_path("Classes/" . $partner['class'] . ".php"))) {
                    return $partner['class'];
                }
            };
        }
        return false;
    }


}