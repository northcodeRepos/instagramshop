<?php

namespace App\Repositories;

use App\Repositories\UserRepository as User;
use App\Repositories\MediaRepository as Media;
use App\Model\Statistics as Stats;

class StatisticsRepository
{

    protected $MediaRepository;
    protected $userRepository;

    /**
     * @param \App\Model\User $user
     * @return void
     */
    public function __construct(Media $MediaRepository, User $userRepository, Stats $statistics)
    {

        $this->media = $MediaRepository;
        $this->user = $userRepository;
        $this->statistics = $statistics;

    }

    public function countVisit($user, $username)
    {
        $this->statistics->saveVisit($user, $username);
    }

    public function countClick($user, $media_id, $username)
    {
        $this->statistics->saveClick($user, $media_id, $username);
    }


}