<?php

namespace App\Repositories;

use App\Model\User;
use MetzWeb\Instagram\Instagram;
use App\Model\Instagram as Insta;
use App\Repositories\MediaRepository as Media;

class UserRepository
{

    protected $MediaRepository;

    /**
     * @param \App\Model\User $user
     * @return void
     */
    public function __construct(Media $MediaRepository)
    {

        $this->media = $MediaRepository;

        $this->instagram = new Instagram(array(
            'apiKey' => env('INSTAGRAM_KEY'),
            'apiSecret' => env('INSTAGRAM_SECRET'),
            'apiCallback' => env('INSTAGRAM_REDIRECT_URI')
        ));

    }


    public function revokeInstagram($user)
    {
        $user->instagram()->delete();
        $user->instagrammedia()->delete();
        $user->statistics()->delete();
    }

    public function connectInstagram($user)
    {

        $authUrl = 'https://api.instagram.com/oauth/authorize';
        $_scopes = array('basic', 'likes', 'public_content');
        $url = $authUrl . '?client_id=' . env('INSTAGRAM_KEY') . '&redirect_uri=' . urlencode(env('INSTAGRAM_REDIRECT_URI')) . '&scope=' . implode('+', $_scopes) . '&response_type=code';
        return json_encode($url);

    }

    public function find($id)
    {
        return User::find($id);
    }

    public function saveInstaProfile($user, $code)
    {

        $data = $this->instagram->getOAuthToken($code);
        if (isset($data->code) && $data->code) return json_encode('error');

        //Check user is have insta connected or no
        if ($user->instagram()->first()) return json_encode('error');
        if (Insta::where('insta_id', $data->user->id)->first()) return json_encode('exist');

        $this->newInstaProfile($user, $data);

        return json_encode('true');

    }

    public function newInstaProfile($user, $data)
    {

        $newInstagram = new Insta();
        $newInstagram->insta_id = $data->user->id;
        $newInstagram->user_id = $user->id;
        $newInstagram->username = $data->user->username;
        $newInstagram->profile_picture = $data->user->profile_picture;
        $newInstagram->full_name = $data->user->full_name;
        $newInstagram->bio = $data->user->bio;
        $newInstagram->website = $data->user->website;
        $newInstagram->save();

        if ($newInstagram->created_at) $this->importInstagramMedia($user);


    }

    public function findInstaProfile($username)
    {
        return User::join('instagrams', 'users.id', '=', 'instagrams.user_id')->where('instagrams.username', $username);
    }

    public function findPublicProfile($user)
    {

        //$url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url=%27https://www.instagram.com/$user->username/%27%20and%20xpath=%27/html/body/script[1]%27&format=json";
        $url = 'http://www.instagram.com/' . $user->username . '/?__a=1';
        $json = file_get_contents($url);
        $obj = json_decode($json, true);
        $publicUser = $obj['user'];
        return $publicUser;

    }

    public function refreshInstagramMedia($user)
    {

        $usermedia = json_decode(file_get_contents('https://igapi.ga/' . $user->first()->instagram()->first()->username . '/media'), true);

        if (count($usermedia['items']) == 0) {
            return json_encode('private_profile');
        }

        $usermedia = collect($usermedia['items']);

        $pushMedia = array();
        $pushMedia = collect($pushMedia);

        foreach ($usermedia as $media) {
            $media = (object)$media;
            $hashtags = NULL;
            if($media->caption['text']) {
                $match_tag = $media->caption['text'];
                $match_tag = preg_match_all('/#([^\s]+)/', $match_tag, $matches);
                $hashtags = implode(',', $matches[1]);
            }
            if (!$this->media->findMedia($media->id)) {

                $pushMedia->push([
                    'user_id' => $user->id,
                    'insta_id' => $user->first()->instagram()->first()->insta_id,
                    'media_id' => $media->id,
                    'code' => $media->code,
                    'image' => urlencode($media->images['standard_resolution']['url']),
                    'hashtags' => $hashtags,
                    'created_time' => $media->created_time,
                    'likes' => $media->likes['count'],
                    'can_view_comments' => $media->can_view_comments,
                    'can_delete_comments' => $media->can_delete_comments,
                    'type' => $media->type,
                    'link' => $media->link
                ]);
            } else {
                $this->media->updateMedia($media->id, $hashtags);
            }
        }

        $this->media->insertMedia($pushMedia->toArray());

    }

    public function importInstagramMedia($user)
    {

        $usermedia = json_decode(file_get_contents('https://igapi.ga/' . $user->instagram()->first()->username . '/media'), true);
        $usermedia = collect($usermedia['items']);

        $pushMedia = array();
        $pushMedia = collect($pushMedia);
        foreach ($usermedia as $media) {
            $media = (object)$media;

            $hashtags = NULL;
            if($media->caption['text']) {
                $match_tag = $media->caption['text'];
                $match_tag = preg_match_all('/#([^\s]+)/', $match_tag, $matches);
                $hashtags = implode(',', $matches);
            }
            $pushMedia->push([
                'user_id' => $user->id,
                'insta_id' => $user->instagram()->first()->insta_id,
                'media_id' => $media->id,
                'code' => $media->code,
                'image' => urlencode($media->images['standard_resolution']['url']),
                'hashtags' => $hashtags,
                'created_time' => $media->created_time,
                'likes' => $media->likes['count'],
                'can_view_comments' => $media->can_view_comments,
                'can_delete_comments' => $media->can_delete_comments,
                'type' => $media->type,
                'link' => $media->link
            ]);
        }
        $this->media->insertMedia($pushMedia->toArray());


    }

}