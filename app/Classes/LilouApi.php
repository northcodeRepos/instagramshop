<?php

namespace app\Classes;

use App\Interfaces\ApiAbstract;
use GuzzleHttp\Client;

class LilouApi extends ApiAbstract {

    /**
     * @param array $products
     * @param mixed $lang
     * @return mixed
     * @throws Exception
     */
    public function getProducts($products = [], $lang = false) {
        try {
            $postData = $this->configuration()['post_data'];
            $postData['id'] = join(',', $products);
            if($lang !== false) {
                $postData['store'] = $lang;
            }

            $client = new Client();
            $res = $client->request($this->configuration()['method'], $this->configuration()['url'], ['form_params' => $postData]);

            if($res->getStatusCode() === 200) {
                return json_decode($res->getBody());
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function configuration() {
        return [
            'url' => 'http://liloudemo.m4u-dev.it/instagram_api.php',
            'method' => 'POST',
            'post_data' => [
                'auth' => 'LwnErewH5VMPh8CePntXJ9Kk',
                'store' => 'pl' //fallback language
            ]
        ];
    }
}