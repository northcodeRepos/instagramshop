<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InstagramMedia extends Model
{

    public function Stats()
    {
        return $this->hasMany('App\Model\Statistics');

    }

    protected $fillable = [
        'user_id', 'insta_id', 'media_id', 'code', 'image', 'created_time', 'hashtags','caption', 'likes', 'hidden', 'custom_link', 'can_view_comments', 'can_delete_comments', 'type', 'link', 'location', 'alt_media_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


    public function insertMedia($media)
    {
        $this::insert($media);
    }

    public static function findMedia($media_id)
    {
        return static::where('media_id', $media_id);
    }

    public function setCustomUrl($media, $url)
    {
        $media->update(['custom_link' => $url]);
    }

    public function updateMedia($media, $hashtags)
    {
        $media->update(['hashtags' => $hashtags]);
    }

    public function showMedia($media)
    {
        $this::find($media->first()->id)->update(['hidden' => NULL]);

    }

    public function hideMedia($media)
    {
        $this::find($media->first()->id)->update(['hidden' => 1]);
    }


}
