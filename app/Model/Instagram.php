<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{

    protected $fillable = [
        'user_id', 'insta_id', 'username', 'profile_picture', 'full_name', 'bio', 'website'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
