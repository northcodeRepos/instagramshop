<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{


    protected $fillable = [
        'instagram_media_id', 'user_id', 'type', 'visited'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function saveVisit($user, $username)
    {

        $saveVisit = new $this;
        $saveVisit->instagram_media_id = 0;
        if ($user) {
            $saveVisit->user_id = $user->first()->id;
        } else {
            $saveVisit->user_id = 0;
        }
        $saveVisit->type = 1;
        $saveVisit->visited = $username;
        $saveVisit->save();
    }

    public function saveClick($user, $media_id, $username)
    {
        $saveVisit = new $this;
        $saveVisit->instagram_media_id = $media_id;
        if ($user) {
            $saveVisit->user_id = $user->first()->id;
        } else {
            $saveVisit->user_id = 0;
        }
        $saveVisit->type = 0;
        $saveVisit->visited = NULL;
        $saveVisit->save();
    }

}
