<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    protected $fillable = [
        'auto_link', 'match_tag','auto_link_url'
    ];

    public function auto_link_on()
    {

       $this::first()->update(['auto_link' => 1]);

    }
    public function auto_link_off()
    {
        $this::first()->update(['auto_link' => null,'auto_link_url' => null]);

    }
    public function save_auto_link_url($url)
    {
        $this::first()->update(['auto_link_url' => $url]);
    }
    public function match_tag_on()
    {
        $this::first()->update(['match_tag' => '[a-zA-Z0-9\-\_]+']);

    }

    public function match_tag_off()
    {
        $this::first()->update(['match_tag' => null]);

    }

    public function save_custom_tag($tag)
    {
        $this::first()->update(['match_tag' => $tag]);
    }
}
